QMAKE_CXXFLAGS += -std=c++11
INCLUDEPATH = ../interfaces 
SOURCES    = src/*.cpp
HEADERS    = src/*.h ../interfaces/*.h
TARGET     = sch_tools
DESTDIR    = ../build
FORMS      = ui/*.ui
UI_DIR     = ui
QT += core gui network
