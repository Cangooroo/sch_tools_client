#ifndef _MAINFORM_H
#define	_MAINFORM_H

#include "../ui/ui_StoreForm.h"
#include "StoreModel.h"

class StoreView : public QMainWindow {
    Q_OBJECT
public:
    StoreView();
    void setModel(QAbstractTableModel* model);
    virtual ~StoreView();
private:
    Ui::MainForm widget;
};

#endif
