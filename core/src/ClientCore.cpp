#include <QHostAddress>
#include <QPluginLoader>
#include <QSettings>
#include <QAction>
#include <QApplication>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <qt4/QtCore/qbytearray.h>
#include <qt4/QtGui/qimage.h>
#include <qt4/QtGui/qpixmap.h>

#include "ClientCore.h"
#include "AppInterface.h"
#include "ToolsMessage.h"

ClientCore::ClientCore(){
    qDebug() << "[CORE] HAI IM ALIV!!!";    // grat

    connectionState = NOT_CONNECTED;
    // A ClientCore konfigurációs fájljának betöltése
    
    QDir dir(qApp->applicationDirPath());
    if(!dir.exists("resources")){
        // Ha nem létezik resources könyvtár, akkor létrehozzuk
        dir.mkdir("resources");
    } 
    QFile file(QString("resources")+QDir::separator()+"client.ini");
    if(!file.exists()){
        // Ha nem létezik a build/resources könyvtárban a konfigurációs fájl, 
        // akkor bemásoljuk.
        // 1.) Megkeressük a másolandó fájlt: a gyökér könyvtárban a resources könyvtárban
        QDir providedIniFileLocation = QDir::currentPath();
        providedIniFileLocation.cdUp();
        providedIniFileLocation.cd("resources");
        QString providedIniFile = providedIniFileLocation.absoluteFilePath("client.ini");
        // 2.) Megadjuk az elérési útját a konfigurációs fájlnak a build könyvtárban
        QDir resourcesDir = QDir::currentPath();
        resourcesDir.cd("resources");
        QString newIniFile = resourcesDir.absoluteFilePath("client.ini");
        // 3.) Átmásoljuk
        QFile::copy(providedIniFile, newIniFile);
    }
    settings = new QSettings(QString("resources")+QDir::separator()+"client.ini", QSettings::IniFormat);
    setHostAddress(settings->value("core/server_address").toString());

    /**
     * SystemTray icon létrehozása.
     * 
     * Ikon színkódok:
     *      - piros: nincs kapcsolat
     *      - sárga: van kapcsolat, nincs bejelentkezve TODO
     *      - zöld: van kapcsolat, be van jelentkezve TODO
     */
    QPixmap pixmap("resources/icon_red.png", "png");
    QIcon icon(pixmap);
    tray = new QSystemTrayIcon(icon);
    
    /**
     * A kezdeti tray menü:
     *      - bejelentkezés
     *      - kilépés
     */
    tray->setContextMenu(&menu);
    
    signInAction = new QAction("Bejelentkezés", this);
    signInForm = new SignInForm(this);
    connect(signInAction, SIGNAL(triggered()), this, SLOT(showSignInWindow()));
    
    showMainWindowAction = new QAction("SCH Tools", this);
    storeView = new StoreView();
    
    exitAction = new QAction("Kilépés", this);
    
    storeView = new StoreView();
    storeView->setModel(new StoreModel(this));
    shopAction = new QAction("Bolt", this);
    connect(shopAction, SIGNAL(triggered()), this, SLOT(showStoreWindow()));
    
    reloadMenus();
    
    /**
     * A tray icon megjelenítése.
     */
    tray->show();
}

void ClientCore::setAppDir(const QDir& applicationDir) {
    appDir = applicationDir;
}

void ClientCore::setHostAddress(const QString& hostAddress) {
    host = hostAddress;
    qDebug() << "[Network] Connecting to " << host << " on port 14000...";
    connect(&serverSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(connectionError()));
    connect(&serverSocket, SIGNAL(connected()), this, SLOT(connectionEstablished()));
    serverSocket.connectToHost(host, 14000);
}

void ClientCore::setParentApp(QApplication* parentApp) {
    app = parentApp;
    // Bekötjük a kikapcsoló gombot
    connect(exitAction, SIGNAL(triggered()), app, SLOT(quit()));
}

void ClientCore::sendMessage(ToolsMessage& message) {    
    QByteArray out;
    QDataStream stream(&out, QIODevice::ReadWrite);
    message.from = schAcc;
    qDebug() << "[Network out] Sending message to" << message.to;
    stream << message.toolID << message.to << message.from << message.data;
    serverSocket.write(out);
}

bool ClientCore::loadApp(const QString& appName) {
    QString file = appName + ".app";
    QPluginLoader pluginLoader(appDir.absoluteFilePath(file));
    qDebug() << appDir.absoluteFilePath(file) << " " << QFile::exists(appDir.absoluteFilePath(file));
    QObject *plugin = pluginLoader.instance();
    if (plugin) {
        AppInterface *appInstance = qobject_cast<AppInterface *>(plugin);
        if (appInstance){
            apps.insert(appInstance->getID(), appInstance);
            qDebug() << "appID: " << appInstance->getID();
            appInstance->setCore(this);
            appInstance->start();
            ToolsMessage message;
            message.to = "core";
            QDataStream stream(&(message.data), QIODevice::ReadWrite);
            stream << QString("tool_loaded") << appInstance->getID();
            sendMessage(message);
        } else {
            qDebug() << "Error while loading module: " << pluginLoader.errorString() << " !";
            return false;
        }
    } else{
        qDebug() << pluginLoader.errorString();
    }
    return true;
}

bool ClientCore::unloadApp(const QString& appID) {
    qDebug() << "ClientCore is going to unload " << appID << "!";
    if (apps.contains(appID)) {
        apps[appID]->stop();
        apps.remove(appID);
        
        ToolsMessage message;
        message.to = "core";
        QDataStream stream(&(message.data), QIODevice::ReadWrite);
        stream << QString("tool_unloaded") << appID;
        sendMessage(message);
    } else {
        qDebug() << appID << " is not among the loaded apps!";
    }
    return true;
}

void ClientCore::connectionEstablished() {
    qDebug() << "[Network] Connection established!";
    /**
     * Már van kapcsolat a szerver felé, küldhetünk neki üzenetet. Ezt tároljuk
     * a connectionState változóban.
     */
    connectionState = NOT_SIGNED_IN;
    
    /**
     * A hálózati kommunikációhoz szükséges SLOT-ok felcsatlakoztatása
     *      - szerver olvasása
     *      - kapcsolat bontásának észlelése
     *      - socket error-ok észlelése
     * 
     * A TCP kapcsolat felállását jelző SIGNAL-ról(QAbstractSocket::connected())
     * lecsatlakozunk
     */
    disconnect(&serverSocket, SIGNAL(connected()),
            this, SLOT(connectionEstablished()));
    connect(&serverSocket, SIGNAL(readyRead()), this, SLOT(readServer()));
    connect(&serverSocket, SIGNAL(disconnected()),
            this, SLOT(connectionClosed()));
    qDebug() << "[Network] Connection with the server is established! schacc = " << schAcc;
    
    
    
    /**
     * A TrayIcont átállítjuk zöldre.
     */
    QPixmap pixmap("resources/icon_blue.png", "png");
    QIcon icon(pixmap);
    tray->setIcon(icon);
    
    /**
     * Újratöltjük a menüt.
     */
    reloadMenus();
}


void ClientCore::readServer() {
    qDebug() << "ClinetCore readServer()";
    QDataStream in(&serverSocket);
    
    /**
     * Beolvassuk az üzenetet a szervertől
     */
    ToolsMessage message;
    in >> message.toolID >> message.to >> message.from >> message.data;
    
    /**
     * Diszpecseljük az üzenetet
     *      - core felé
     *      - valamelyik tool felé (ha létezik megfelelő ID-jú)
     * 
     * A kliensben még nem foglalkozunk azzal, ha olyan toolID-jú üzenet jött,
     * amilyen tool-unk nincs.
     * 
     * Később meg kell majd oldanunk, hogy egy szervertool broadcast üzenetet
     * csak azoknak a klienseknek küldjön, akiknél megvan az adott tool
     * (biztonsági, és teljesítmény okok miatt)
     */
    if(message.toolID=="CORE"){
        receiveMessage(message);
    } else{
        if(apps.contains(message.toolID)){
            apps[message.toolID]->receiveMessage(message);
        } else{
            qDebug() << "[Network in] Doesn't have tool with ID " << message.toolID;
        }
    }
    serverSocket.readAll();
}

void ClientCore::connectionError() {
    qDebug() << "[Network error] ClientCore Socket Error: " << serverSocket.errorString();
}

bool ClientCore::isConnected() {
    return connectionState;
}

void ClientCore::connectionClosed() {
    qDebug() << "[Network disconnect] Server has disconnected!";
    QPixmap oPixmap(16, 16);
    oPixmap.fill(qRgb(255, 10, 10));
    QIcon icon(oPixmap);
    tray->setIcon(icon);
    // TODO
    // Beállítani valamit, ami újracsatlakozik(megpróbál) 30 másodpercenként.
}

void ClientCore::reloadAllTools() {
    // TODO
}

void ClientCore::showSignInWindow() {
    signInForm->show();
}

void ClientCore::showStoreWindow() {
    storeView->show();
}

void ClientCore::reloadMenus() {
    switch(connectionState){
        case NOT_CONNECTED:
            menu.clear();
            menu.addAction(shopAction);
            menu.addAction(exitAction);
            break;
        case NOT_SIGNED_IN:
            menu.clear();
            menu.addAction(signInAction);
            menu.addAction(exitAction);
            break;
        case SIGNED_IN:
            menu.clear();
            menu.addAction(shopAction);
            /**
             * Tool-ok
             */
            for(QHash<QString, AppInterface*>::iterator it = apps.begin();
            it!=apps.end(); ++it){
                AppInterface *appInstance = it.value();
                QMenu *subMenu = appInstance->getMenu();
                if (subMenu != NULL){
                    menu.addMenu(subMenu);
                }
            }
            /**
             * Exit
             */
            menu.addAction(exitAction);
            break;
    }
}

void ClientCore::signIn(QString account) {
    schAcc = account;
    ToolsMessage message;
    message.toolID = "CORE";
    message.to = "core";
    QDataStream stream(&(message.data), QIODevice::ReadWrite);
    stream << QString("sign_in");
    sendMessage(message);
}

void ClientCore::receiveMessage(ToolsMessage& message) {
    qDebug() << "[Network in] CORE has recieved a message";
    QDataStream stream(&(message.data), QIODevice::ReadWrite);
    QString messageType;
    stream >> messageType;
    if(messageType=="login_success"){
        // TODO új függvény
        connectionState = SIGNED_IN;
        QStringList loadOnStartup = settings->value("apps/load", "").toString().split(",");
        for (QString appName : loadOnStartup) {
            if (appName.isEmpty())  // ures string eseten ne akarjon loadolni
                continue;
            loadApp(appName);
        }
        reloadMenus();
        QPixmap pixmap("resources/icon_green.png", "png");
        QIcon icon(pixmap);
        tray->setIcon(icon);
    }
}
