#ifndef CLIENTCORE_H
#define	CLIENTCORE_H

#include <QtNetwork/QTcpSocket>
#include <QObject>
#include <ClientCoreInterface.h>
#include <QDir>
#include <QHostAddress>
#include <QSettings>
#include <QSystemTrayIcon>
#include <AppInterface.h>

#include "SignInForm.h"
#include "StoreView.h"

class ClientCore : public ClientCoreInterface {
    Q_OBJECT
public:
    /**
     * Konstruktor. 'nuff said.
     */
    ClientCore();
    
    /**
     * A tulajdonos QApplication-ra pointer.
     * Azért van rá szükség, hogy mi mondhassuk meg,
     * mikor van vége a szoftver futásának. 
     * @param parentApp Pointer a QApplication objektumra.
     */
    void setParentApp(QApplication* parentApp);
    
    /**
     * Az app-okat tároló könyvtárra mutat. Azért van rá szükség,
     * hogy a loadApp()-nak csak az app nevét kelljen megadni.
     * @param applicationDir Az appokat tároló könyvátrra mutató QDir objektum
     */
    void setAppDir(const QDir& applicationDir);
    
    /**
     * A szerver IP címe.
     * @param hostAddress A szerver IP címét tartalmazó QString
     */
    void setHostAddress(const QString& hostAddress);
    
    /**
     * A kliensnek mondjuk meg, hogy töltsön be egy app-ot.
     * @param appName A betöltendő alkalmazás neve. Ugyanaz, ami
     * a fájlnévben is szerepel.
     * @return A betöltés sikeres volt-e. tru, ha sikeres, false, ha sikertelen.
     */
    bool loadApp(const QString& fileName);
    
    /**
     * Leállítja, és bezárja az appot.
     * @param appID A leállítandó alkalmazás ID-ja.
     * @return true, ha sikeresen leállt, false egyébként.
     */
    bool unloadApp(const QString& appID);
    
    /**
     * Adat küldése a szerver alkalmazás felé. Aszinkron a hálózat miatt.
     * @param toolID message a küldendő bájtok tömbje.
     * @param message
     */
    void sendMessage(ToolsMessage& message);
    
    /**
     * A szerver<->kliens kapcsolat állapotát kérdezhetjük le vele. Magyarán
     * tudunk-e üzenetet küldeni a szervernek.
     * @return A kapcsolat állapota(true=kapcsolódva, false=nincs kapcsolat)
     */
    bool isConnected();

private slots:
    /**
     * Betölti (újra) az összes tool-t, ami a konfigurációs fájl apps/load
     * mezőjében meg van adva.
     */
    void reloadAllTools();
    
    /**
     * Megjeleníti a bejelentkező ablakot.
     */
    void showSignInWindow();
    
    /**
     * Megjeleníti az alkalmazásboltot.
     */
    void showStoreWindow();
    
    /**
     * Feltölti a SystemTray menüt annak megfelelően, hogy mi a kapcsolat
     * állapota.
     */
    void reloadMenus();
    
public slots:
    /**
     * [SLOT] Ha valami para van a sockettel a szerver felé, akkor ez hívódik meg
     */
    void connectionError();
    
    /**
     * [SLOT] Ha olvasandó adat áll rendelkezésre a szervertől, akkor ez hívódik meg.
     */
    void readServer();
    
    /**
     * [SLOT] Meghívódik, amikor a serverSocket sikeresen kapcsolódott a szerverhez
     */
    void connectionEstablished();
    
    /**
     * [SLOT] Meghívódik, amikor a szerver bezárja a kapcsolatot
     */
    void connectionClosed();
    
    /**
     * A szervertől a Clientcore-nak küldött üzenet.
     * @param message A kapőott message
     */
    void receiveMessage(ToolsMessage& message);
    
    /**
     * Bejelentkezési művelet.
     */
    void signIn(QString account);

private:
    /**
     * Az appokat tartalmazó könyvtár.
     */
    QDir appDir;
    
    /**
     * Az appok ID szerint tárolva.
     */
    QHash<QString, AppInterface*> apps;
    
    /**
     * Socket a szerver felé
     */
    QTcpSocket serverSocket;
    
    /**
     * A host IP címe
     */
    QString host;
    
    /**
     * A tálcán lévő ikon
     */
    QSystemTrayIcon *tray;
    
    /**
     * A tulajdonos QApplication alkalmazás
     */
    QApplication* app;
    
    /**
     * A tálcán lévő ikon menüje
     */
    QMenu menu;
    
    /**
     * A client.ini fájlban lévő beállításokat tudjuk írni/olvasni.
     */
    QSettings *settings;
    
    /**
     * A kliens "schAcc"-ja. Csak ideiglenesen van bent.
     */
    QString schAcc;
    
    /**
     * A szerver<->kliens kapcsoalt állapota
     */
    ConnectionState connectionState;
    
    /**
     * A menüben lévő Exit "gomb"
     */
    QAction *exitAction;
    
    /**
     * A menüben lévő Bolt "gomb"
     */
    QAction *shopAction;
    
    /*
     * Az alkalmazásbolt.
     */
    StoreView *storeView;
    
    /**
     * A menüben lévő Bejelentkezés "gomb"
     */
    QAction *signInAction;
    
    /**
     * A bejelentkező ablak
     */
    SignInForm *signInForm;
    
    /**
     * A menüben lévő SCH Tools "gomb"
     */
    QAction *showMainWindowAction;
};

#endif
