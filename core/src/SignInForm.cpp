#include <QLineEdit>
#include <QDebug>
#include "SignInForm.h"

SignInForm::SignInForm(ClientCoreInterface *client) {
    core = client;
    widget.setupUi(this);
    connect(widget.pushButton, SIGNAL(clicked()), this, SLOT(signIn()));
}

void SignInForm::signIn() {
    core->signIn(widget.lineEdit->text());
    this->hide();
}
