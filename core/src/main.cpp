#include <QApplication>
#include <QTextCodec>
#include <QHostAddress>

#include "ClientCore.h"

int main(int argc, char *argv[]) {
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    
    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(false);
    
    QString host("127.0.0.1");
    QDir pluginsDir(qApp->applicationDirPath());
    pluginsDir.cd("tools");
    
    ClientCore client;
    client.setParentApp(&app);
    client.setAppDir(pluginsDir);

    return app.exec();
}
