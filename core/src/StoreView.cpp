#include <qt4/QtGui/qheaderview.h>

#include "StoreView.h"

StoreView::StoreView() {
    widget.setupUi(this);
    widget.tableView->verticalHeader()->hide();
    widget.tableView->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
}

void StoreView::setModel(QAbstractTableModel* model) {
    widget.tableView->setModel(model);
}


StoreView::~StoreView() {
}
