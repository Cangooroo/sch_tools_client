#ifndef _SIGNINFORM_H
#define	_SIGNINFORM_H

#include "../ui/ui_SignInForm.h"
#include <ClientCoreInterface.h>

class SignInForm : public QDialog {
    Q_OBJECT
public:
    SignInForm(ClientCoreInterface *client);
private slots:
    void signIn();
private:
    Ui::SignInForm widget;
    ClientCoreInterface *core;
};

#endif
