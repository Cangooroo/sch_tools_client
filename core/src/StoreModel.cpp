#include "StoreModel.h"

StoreModel::StoreModel(QObject* parent) : QAbstractTableModel(parent) {
}

int StoreModel::rowCount(const QModelIndex& parent) const {
    return 8;
}

int StoreModel::columnCount(const QModelIndex& parent) const {
    return 4;
}

QVariant StoreModel::data(const QModelIndex& index, int role) const {
    switch (role) {
        case Qt::DisplayRole:
            switch (index.column()) {
                case 0:
                    return QString("Tool Name");
                    break;
                case 1:
                    return QString("Rövid leírás");
                    break;
                case 2:
                    return QString("Nincs telepítve");
                    break;
            }
            break;
        case Qt::CheckStateRole:
            if (index.column() == 3) {
                    return Qt::Unchecked;
            }
            break;
    }
    return QVariant();
}

QVariant StoreModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            switch (section) {
                case 0:
                    return QString("Név");
                    break;
                case 1:
                    return QString("Leírás");
                    break;
                case 2:
                    return QString("Állapot");
                    break;
                case 3:
                    return QString("Telepítve");
                    break;
            }
        }
    }
    return QVariant();
}

Qt::ItemFlags StoreModel::flags(const QModelIndex& index) const {
    return Qt::ItemIsEnabled;
}
