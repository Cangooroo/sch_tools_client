#ifndef STOREMODEL_H
#define	STOREMODEL_H

#include <QAbstractTableModel>

class StoreModel : public QAbstractTableModel {
    Q_OBJECT
public:
    StoreModel(QObject *parent = 0);
    
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    
    virtual Qt::ItemFlags flags(const QModelIndex& index) const;
};

#endif
