TEMPLATE        = lib
CONFIG += plugin no_plugin_name_prefix 
QMAKE_EXTENSION_SHLIB = app
QMAKE_CXXFLAGS += -std=c++11
INCLUDEPATH    += ../../interfaces
HEADERS         = src/*.h ../../interfaces 
SOURCES         = src/*.cpp
TARGET          = SCHat
DESTDIR         = ../../build/tools
FORMS           = ui/*.ui
UI_DIR          = ui
QT += core network gui