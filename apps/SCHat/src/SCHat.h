#ifndef SCHAT_H
#define	SCHAT_H

#include <AppInterface.h>
#include <ClientCoreInterface.h>

#include "../ui/ui_ChatForm.h"

class SCHat : public AppInterface{
    Q_OBJECT
    Q_INTERFACES(AppInterface)
public:
    SCHat();
    QString getID() const;
    QMenu* getMenu();
    void run();
    void stop();
    virtual void receiveMessage(ToolsMessage& message);
    virtual void setCore(QObject* coreInterface);

protected slots:
    void postMessage();
    
private:
    ClientCoreInterface *core;   
    QString toolID;
    bool stopped;
    Ui::ChatForm widget;
    QDialog *dialog;
};

#endif	/* SCHAT_H */

