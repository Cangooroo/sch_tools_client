#include "SCHat.h"
#include "ui_ChatForm.h"
#include <QDataStream>
#include <qplugin.h>
#include <ToolsMessage.h>
#include <qt4/QtGui/qdialog.h>

SCHat::SCHat() {
    toolID = "APP_SCHAT";
    stopped = false;
    core = NULL;
    dialog = new QDialog();
    widget.setupUi(dialog);
    widget.plainTextEdit->appendPlainText("[Server] Hai! Wlekom tu SCtHat!\n[Server] y u du dissss?");
    connect(widget.pushButton, SIGNAL(clicked()), this, SLOT(postMessage()));
    dialog->show();
}

QMenu* SCHat::getMenu() {
    return NULL;
}

QString SCHat::getID() const {
    return toolID;
}

void SCHat::run() {
    while(!stopped){
        sleep(100);
    }
}

void SCHat::stop() {
    stopped = true;
}

void SCHat::setCore(QObject* coreInterface) {
    core = (ClientCoreInterface*) coreInterface;
}

void SCHat::receiveMessage(ToolsMessage& message) {
    QDataStream stream(&(message.data), QIODevice::ReadOnly);
    QString tmp;
    stream >> tmp;
    qDebug() << "SCHat received message(from:" << message.from << "): " << tmp;
    
    QString msg = "[" + message.from + "] " + tmp;
    widget.plainTextEdit->appendPlainText(msg);
}

void SCHat::postMessage(){
    ToolsMessage message;
    message.toolID = toolID;
    message.to = widget.lineEdit->text();
    QDataStream stream(&(message.data), QIODevice::ReadWrite);
    QString msg;
    msg = widget.lineEdit_2->text();
    widget.lineEdit_2->clear();
    stream << msg;
    core->sendMessage(message);   
}


Q_EXPORT_PLUGIN2(SCHat, SCHat); // (plugin name, plugin class) 