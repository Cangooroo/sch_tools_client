#ifndef APPINTERFACE_H
#define	APPINTERFACE_H

#include <qplugin.h>
#include <QThread>
#include <QMenu>

#include "ToolsMessage.h"

class AppInterface : public QThread{
public:
    virtual void setCore(QObject *coreInterface) = 0;
    
    /**
     * A QThread miatt kell. Ez a szálnak a főciklusa
     */
    virtual void run() = 0;
    
    /**
     * A szál megállítása. Gyakorlatilag a stopped változó true-ra állítása
     */
    virtual void stop() = 0;
    
    /**
     * Az app ID-ját tudjuk tőle lekérdezni. pl.: "APP_ASDASDASD"
     * @return Az app ID-ja
     */
    virtual QString getID() const = 0;
    
    /**
     * A tálcán lévő ikonra kattintva előjön egy menü, amiben az alkalmazások
     * vannak felsorolva.
     * Ezzel az objektummal tudjuk megadni, hogy:
     *   - szeretnénk-e felsorolva lenni a menüben(ha nem, akkor NULL-lal
     * térünk vissza)
     *   - szeretnénk-e almenüt
     * A tálcán lévő icon menüje is egy ugyanilyen QMenu. A QMenu-k egymásba
     * ágyazhatók, így tudunk létrehozni egy hierarchikus szerkezetet.
     * @return Az általunk összeállított menü
     */
    virtual QMenu* getMenu() = 0;
    
    virtual void receiveMessage(ToolsMessage& message) = 0;
};

Q_DECLARE_INTERFACE(AppInterface,
        "coder.tools.client.AppInterface/1.0")

#endif
