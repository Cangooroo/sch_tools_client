#ifndef MESSAGE_H
#define	MESSAGE_H

#include <QByteArray>

struct ToolsMessage{
    /*
     * Message irányok lehetnek:
     *      ClientCore <-> ServerCore
     *      szerver oldali tool <-> kliens oldali tool
     *      szerver oldali tool <-> ClientCore
     *      kliens oldali tool <-> ServerCore
     * 
     * Ha a cél egy core(ServerCore, ClientCore), akkor a ToolsMessage.to
     * részben "core" szerepel. Minden egyéb esetben az üzenet tool-nak megy.
     * 
     * A P2P-s kommunikációkban nem használjuk ezt az üzenet típust(mivel pl.
     * fájl átvitelnél lehet, hogy visszafogna minket egy ilyen csomagba bezárni
     * a byte-okat), így nem fordul elő, hogy ToolsMessage kliens<->kliens közt
     * menjen.
     */
    QString toolID; // ezt akkor vesszük figyelembe, ha nem core-nak megy üzenet
    QString from;   // "core", "tool", {{schacc}}
    QString to;     // "core", "tool", {{schacc}}, "all"
    QByteArray data;
};

#endif

