#ifndef CLIENTCOREINTERFACE_H
#define	CLIENTCOREINTERFACE_H

#include <qplugin.h>
#include <QDir>
#include <QtNetwork/QTcpSocket>
#include <QApplication>
#include <ToolsMessage.h>

class ClientCoreInterface : public QObject{
    Q_OBJECT
    
public:
    typedef enum eConnectionState{
        NOT_CONNECTED,
        NOT_SIGNED_IN,
        SIGNED_IN
    }ConnectionState;
    
public:
    /**
     * A tulajdonos QApplication-ra pointer.
     * Azért van rá szükség, hogy mi mondhassuk meg,
     * mikor van vége a szoftver futásának. 
     * @param parentApp Pointer a QApplication objektumra.
     */
    virtual void setParentApp(QApplication* parentApp) = 0;
    
    /**
     * Az app-okat tároló könyvtárra mutat. Azért van rá szükség,
     * hogy a loadApp()-nak csak az app nevét kelljen megadni.
     * @param applicationDir Az appokat tároló könyvátrra mutató QDir objektum
     */
    virtual void setAppDir(const QDir& applicationDir) = 0;
    
    /**
     * A szerver IP címe.
     * @param hostAddress A szerver IP címét tartalmazó QString
     */
    virtual void setHostAddress(const QString& hostAddress) = 0;
    
    /**
     * A kliensnek mondjuk meg, hogy töltsön be egy app-ot.
     * @param appName A betöltendő alkalmazás neve. Ugyanaz, ami
     * a fájlnévben is szerepel.
     * @return A betöltés sikeres volt-e. tru, ha sikeres, false, ha sikertelen.
     */
    virtual bool loadApp(const QString &appName) = 0;
    
    /**
     * Leállítja, és bezárja az appot.
     * @param appID A leállítandó alkalmazás ID-ja.
     * @return true, ha sikeresen leállt, false egyébként.
     */
    virtual bool unloadApp(const QString &appID) = 0;
    
    /**
     * Adat küldése a szerver alkalmazás felé. Aszinkron a hálózat miatt.
     * @param message a küldendő bájtok tömbje.
     */
    virtual void sendMessage(ToolsMessage& message) = 0;
    
    /**
     * A szerver<->kliens kapcsolat állapotát kérdezhetjük le vele. Magyarán
     * tudunk-e üzenetet küldeni a szervernek.
     * @return A kapcsolat állapota(true=kapcsolódva, false=nincs kapcsolat)
     */
    virtual bool isConnected() = 0;

public slots:
    /**
     * Ha valami para van a sockettel a szerver felé, akkor ez hívódik meg
     */
    virtual void connectionError() = 0;
    
    /**
     * Ha olvasandó adat áll rendelkezésre a szervertől, akkor ez hívódik meg.
     */
    virtual void readServer() = 0;
    
    /**
     * Bejelentkezési művelet.
     */
    virtual void signIn(QString account) = 0;
};

Q_DECLARE_INTERFACE(ClientCoreInterface,
        "coder.schtools.ClientCoreInterface/0.9")

#endif
